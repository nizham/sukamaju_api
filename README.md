# sukamaju_api

back-end for sukamaju application

## Persayaratan:
- JDK11 (Java Development Kit versi 11)
- PostgreSQL versi 14.2

Langkah-langkah menjalankan aplikasi:
- setup postgresql username postgres tanpa password
- masuk ke database Postgresql dan buat databasenya dengan perintah: create database sukamaju;
- clone repository ini
- masuk ke folder project ini (sukamaju_api)
- ketik perintah berikut:
  - ./mvn spring-boot:run
  - buka browser dan ketik alamat "http://localhost:8080/swagger-ui/index.html"
  - setelah bisa di test API nya, silakan jalankan aplikasi "sukamaju", front-end dari aplikasi ini