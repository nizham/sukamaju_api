package com.coach.sukamaju.api;

import com.coach.sukamaju.model.Pelanggan;
import com.coach.sukamaju.model.Produk;
import com.coach.sukamaju.model.Transaksi;
import com.coach.sukamaju.service.TransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class TransaksiCtrl {
    @Autowired
    private TransaksiService transaksiSrv;

    @PostMapping("/transaksi")
    public ResponseEntity<String> create(@RequestBody Transaksi transaksi) {
        return new ResponseEntity<>(transaksiSrv.createOrUpdate(transaksi), HttpStatus.OK);
    }

    @GetMapping("/transaksi")
    public ResponseEntity<List<Transaksi>> retrieve() {
        List<Transaksi> transaksiList = transaksiSrv.readAll();
        if (transaksiList.isEmpty()) {
            return new ResponseEntity<>(transaksiList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(transaksiList, HttpStatus.OK);
        }
    }

    @PutMapping("/transaksi")
    public ResponseEntity<String> update(@RequestBody Transaksi transaksi) {
        return new ResponseEntity<>(transaksiSrv.createOrUpdate(transaksi), HttpStatus.OK);
    }

    @DeleteMapping("/transaksi/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Transaksi transaksi = transaksiSrv.read(id).get();
        return new ResponseEntity<>(transaksiSrv.delete(transaksi), HttpStatus.OK);
    }
}
