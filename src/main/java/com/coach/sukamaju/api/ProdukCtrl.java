package com.coach.sukamaju.api;

import com.coach.sukamaju.model.Produk;
import com.coach.sukamaju.service.ProdukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class ProdukCtrl {
    @Autowired
    private ProdukService produkSrv;

    @PostMapping("/produk")
    public ResponseEntity<String> create(@RequestBody Produk produk) {
        return new ResponseEntity<>(produkSrv.createOrUpdate(produk), HttpStatus.OK);
    }

    @GetMapping("/produk")
    public ResponseEntity<List<Produk>> retrieve() {
        List<Produk> produkList = produkSrv.readAll();
        if (produkList.isEmpty()) {
            return new ResponseEntity<>(produkList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(produkList, HttpStatus.OK);
        }
    }

    @PutMapping("/produk")
    public ResponseEntity<String> update(@RequestBody Produk produk) {
        return new ResponseEntity<>(produkSrv.createOrUpdate(produk), HttpStatus.OK);
    }

    @DeleteMapping("/produk/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        String message = "";
        if (produkSrv.read(id).isPresent()) {
            Produk produk = produkSrv.read(id).get();
            message = "Berhasil hapus produk";
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
