package com.coach.sukamaju.api;

import com.coach.sukamaju.model.Pelanggan;
import com.coach.sukamaju.service.PelangganService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class PelangganCtrl {
    @Autowired
    private PelangganService pelangganSrv;

    @PostMapping("/pelanggan")
    public ResponseEntity<String> create(@RequestBody Pelanggan pelanggan) {
        return new ResponseEntity<>(pelangganSrv.createOrUpdate(pelanggan), HttpStatus.OK);
    }

    @GetMapping("/pelanggan")
    public ResponseEntity<List<Pelanggan>> retrieve() {
        List<Pelanggan> pasienList = pelangganSrv.readAll();
        if (pasienList.isEmpty()) {
            return new ResponseEntity<>(pasienList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(pasienList, HttpStatus.OK);
        }
    }

    @PutMapping("/pelanggan")
    public ResponseEntity<String> update(@RequestBody Pelanggan pelanggan) {
        return new ResponseEntity<>(pelangganSrv.createOrUpdate(pelanggan), HttpStatus.OK);
    }

    @DeleteMapping("/pelanggan/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        if (pelangganSrv.read(id).isPresent()) {
            Pelanggan pelanggan = pelangganSrv.read(id).get();
            return new ResponseEntity<>(pelangganSrv.delete(pelanggan), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Pelanggan tidak ditemukan", HttpStatus.NOT_FOUND);
        }
    }
}
