package com.coach.sukamaju.api;

import com.coach.sukamaju.model.DetailTransaksi;
import com.coach.sukamaju.model.Pelanggan;
import com.coach.sukamaju.model.Produk;
import com.coach.sukamaju.service.DetailTransaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class DetailTransaksiCtrl {
    @Autowired
    private DetailTransaksiService detailTransaksiSrv;

    @PostMapping("/detailtransaksi")
    public ResponseEntity<String> create(@RequestBody DetailTransaksi detailTransaksi) {
        return new ResponseEntity<>(detailTransaksiSrv.createOrUpdate(detailTransaksi), HttpStatus.OK);
    }

    @GetMapping("/detailtransaksi")
    public ResponseEntity<List<DetailTransaksi>> retrieve() {
        List<DetailTransaksi> detailTransaksiList = detailTransaksiSrv.readAll();
        if (detailTransaksiList.isEmpty()) {
            return new ResponseEntity<>(detailTransaksiList, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(detailTransaksiList, HttpStatus.OK);
        }
    }

    @PutMapping("/detailtransaksi")
    public ResponseEntity<String> update(@RequestBody DetailTransaksi detailTransaksi) {
        return new ResponseEntity<>(detailTransaksiSrv.createOrUpdate(detailTransaksi), HttpStatus.OK);
    }

    @DeleteMapping("/detailtransaksi/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        DetailTransaksi detailTransaksi = detailTransaksiSrv.read(id).get();
        return new ResponseEntity<>(detailTransaksiSrv.delete(detailTransaksi), HttpStatus.OK);
    }
}
