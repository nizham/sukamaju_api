package com.coach.sukamaju.service;

import com.coach.sukamaju.model.Produk;
import com.coach.sukamaju.model.Transaksi;
import com.coach.sukamaju.repo.ProdukRepo;
import com.coach.sukamaju.repo.TransaksiRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransaksiService {
    Logger logger = LoggerFactory.getLogger(TransaksiService.class);

    @Autowired
    private TransaksiRepo transaksiRepo;
    private String pesan = "";

    public String createOrUpdate(Transaksi transaksi) {
        try {
            transaksiRepo.save(transaksi);
            pesan = "Berhasil simpan transaksi";
            logger.info(pesan);
        } catch (Exception e) {
            pesan = "Gagal simpan transaksi";
            logger.error(pesan, e);
        }
        return pesan;
    }

    public List<Transaksi> readAll() {
        List<Transaksi> transaksiList = new ArrayList<>();
        try {
            transaksiRepo.findAll().forEach(p -> transaksiList.add(p));
            logger.info("Berhasil mengambil data");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return transaksiList;
    }

    public Optional<Transaksi> read(Long id) {
        return transaksiRepo.findById(id);
    }

    public String delete(Transaksi transaksi) {
        try {
            transaksiRepo.delete(transaksi);
            pesan = "Berhasil hapus transaksi";
            logger.info(pesan);
            return pesan;
        } catch (Exception e) {
            pesan = "Gagal hapus transaksi";
            logger.error(pesan, e.getMessage());
            return pesan;
        }
    }
}
