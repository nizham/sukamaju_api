package com.coach.sukamaju.service;

import com.coach.sukamaju.model.Pelanggan;
import com.coach.sukamaju.repo.PelangganRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PelangganService {
    Logger logger = LoggerFactory.getLogger(PelangganService.class);

    @Autowired
    private PelangganRepo pelangganRepo;
    private String pesan = "";

    public String createOrUpdate(Pelanggan pelanggan) {
        try {
            pelangganRepo.save(pelanggan);
            pesan = "Berhasil simpan pelanggan";
            logger.info(pesan);
        } catch (Exception e) {
            pesan = "Gagal simpan pelanggan";
            logger.error(pesan, e);
        }
        return pesan;
    }

    public List<Pelanggan> readAll() {
        List<Pelanggan> pelangganList = new ArrayList<>();
        try {
            pelangganRepo.findAll().forEach(p -> pelangganList.add(p));
            logger.info("Berhasil mengambil data");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return pelangganList;
    }

    public Optional<Pelanggan> read(Long id) {
        return pelangganRepo.findById(id);
    }

    public String delete(Pelanggan pelanggan) {
        try {
            pelangganRepo.delete(pelanggan);
            pesan = "Berhasil hapus pelanggan";
            logger.info(pesan);
        } catch (Exception e) {
            pesan = "Gagal hapus pelanggan";
            logger.error(pesan, e.getMessage());
        }
        return pesan;
    }
}
