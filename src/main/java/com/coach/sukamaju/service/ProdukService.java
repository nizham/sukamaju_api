package com.coach.sukamaju.service;

import com.coach.sukamaju.model.Produk;
import com.coach.sukamaju.repo.ProdukRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProdukService {
    Logger logger = LoggerFactory.getLogger(ProdukService.class);

    @Autowired
    private ProdukRepo produkRepo;
    private String pesan = "";

    public String createOrUpdate(Produk produk) {
        try {
            produkRepo.save(produk);
            pesan = "Berhasil simpan produk";
            logger.info(pesan);
        } catch (Exception e) {
            pesan = "Gagal simpan produk";
            logger.error(pesan, e);
        }
        return pesan;
    }

    public List<Produk> readAll() {
        List<Produk> produkList = new ArrayList<>();
        try {
            produkRepo.findAll().forEach(p -> produkList.add(p));
            logger.info("Berhasil mengambil data");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return produkList;
    }

    public Optional<Produk> read(Long id) {
        return produkRepo.findById(id);
    }

    public String delete(Produk produk) {
        try {
            produkRepo.delete(produk);
            pesan = "Berhasil hapus produk";
            logger.info(pesan);
            return pesan;
        } catch (Exception e) {
            pesan = "Gagal hapus produk";
            logger.error(pesan, e.getMessage());
            return pesan;
        }
    }
}
