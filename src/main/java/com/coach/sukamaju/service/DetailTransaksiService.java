package com.coach.sukamaju.service;

import com.coach.sukamaju.model.DetailTransaksi;
import com.coach.sukamaju.repo.DetailTransaksiRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DetailTransaksiService {
    Logger logger = LoggerFactory.getLogger(DetailTransaksiService.class);

    @Autowired
    private DetailTransaksiRepo detailTransaksiRepo;
    private String pesan = "";

    public String createOrUpdate(DetailTransaksi detailTransaksi) {
        try {
            detailTransaksiRepo.save(detailTransaksi);
            pesan = "Berhasil simpan detail transaksi";
            logger.info(pesan);
        } catch (Exception e) {
            pesan = "Gagal simpan detail transaksi";
            logger.error(pesan, e);
        }
        return pesan;
    }

    public List<DetailTransaksi> readAll() {
        List<DetailTransaksi> detailTransaksiList = new ArrayList<>();
        try {
            detailTransaksiRepo.findAll().forEach(p -> detailTransaksiList.add(p));
            logger.info("Berhasil mengambil data");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return detailTransaksiList;
    }

    public Optional<DetailTransaksi> read(Long id) {
        return detailTransaksiRepo.findById(id);
    }

    public String delete(DetailTransaksi detailTransaksi) {
        try {
            detailTransaksiRepo.delete(detailTransaksi);
            pesan = "Berhasil hapus detail transaksi";
            logger.info(pesan);
            return pesan;
        } catch (Exception e) {
            pesan = "Gagal hapus detail transaksi";
            logger.error(pesan, e.getMessage());
            return pesan;
        }
    }
}
