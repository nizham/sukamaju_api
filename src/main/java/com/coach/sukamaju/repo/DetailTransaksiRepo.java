package com.coach.sukamaju.repo;

import com.coach.sukamaju.model.DetailTransaksi;
import org.springframework.data.repository.CrudRepository;

public interface DetailTransaksiRepo extends CrudRepository<DetailTransaksi, Long> {
}
