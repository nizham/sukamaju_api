package com.coach.sukamaju.repo;

import com.coach.sukamaju.model.Produk;
import org.springframework.data.repository.CrudRepository;

public interface ProdukRepo extends CrudRepository<Produk, Long> {
}
