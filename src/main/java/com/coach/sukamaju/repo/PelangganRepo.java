package com.coach.sukamaju.repo;

import com.coach.sukamaju.model.Pelanggan;
import org.springframework.data.repository.CrudRepository;

public interface PelangganRepo extends CrudRepository<Pelanggan, Long> {
}
