package com.coach.sukamaju.repo;

import com.coach.sukamaju.model.Transaksi;
import org.springframework.data.repository.CrudRepository;

public interface TransaksiRepo extends CrudRepository<Transaksi, Long> {
}
