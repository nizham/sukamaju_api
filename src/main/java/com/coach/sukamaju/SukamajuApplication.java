package com.coach.sukamaju;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SukamajuApplication {

	public static void main(String[] args) {
		SpringApplication.run(SukamajuApplication.class, args);
	}

}
